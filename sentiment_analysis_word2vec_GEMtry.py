import pandas as pd
from nltk.corpus import stopwords
#for regular expressions
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer
import re

porter_stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()

train = pd.read_csv("res/GEM.tsv",header=0,delimiter="\t",quoting=3)

#print column names
#print(train.columns.values)

def review_wordlist(review, remove_stopwords=False):
    # Removing non-letter.
    review_text = re.sub("[^a-zA-Z]", " ", review)
    # Converting to lower case and splitting
    words = review_text.lower().split()
    # Optionally remove stopwords
    if remove_stopwords:
        stops = set(stopwords.words("english"))
        words = [w for w in words if not w in stops]
    # Stemming
    words1 = [porter_stemmer.stem(w) for w in words]
    # Lemming
    words2 = [wordnet_lemmatizer.lemmatize(w) for w in words1]
    # list of words to string
    #sentence = ' '.join(word for word in words2)
    return (words2)

import nltk.data
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

# This function splits a review into sentences
def review_sentences(review, tokenizer, remove_stopwords=True):
    # 1. Using nltk tokenizer
    raw_sentences = tokenizer.tokenize(review.strip())
    #print(raw_sentences)
    sentences = []
    # 2. Loop for each sentence
    for raw_sentence in raw_sentences:
        if len(raw_sentence)>0:
            sentences.append(review_wordlist(raw_sentence,remove_stopwords))
    # This returns the list of lists
    return sentences

sentences = []
print("Parsing sentences from training set")
for row in train["question4"]:
    if type(row)==float:
        row=str(row)
    sentences += review_sentences(row, tokenizer)

# Importing the built-in logging module
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
# Creating the model and setting values for the various parameters
num_features = 300  # Word vector dimensionality
min_word_count = 40 # Minimum word count
num_workers = 4     # Number of parallel threads
context = 10        # Context window size
downsampling = 1e-3 # (0.001) Downsample setting for frequent words

# Initializing the train model
from gensim.models import word2vec
print("Training model....")
print(len(sentences))
model = word2vec.Word2Vec(sentences,
                          workers=num_workers,
                          size=num_features,
                          min_count=min_word_count,
                          window=context,
                          sample=downsampling)

# To make the model memory efficient
model.init_sims(replace=True)

# Saving the model for later use. Can be loaded using Word2Vec.load()
#model_name = "300features_40minwords_10context"
model.save("model_name")

# This will print the most similar words present in the model

#print(model.wv.syn0.shape)
#print(model.wv.vocab)
print(model.wv.most_similar("richard"))
"""
preprocessed_column=[]
for answer in train["question4"]:
    if type(answer) == float:
        answer = str(answer)
    preprocessed_column.append(review_wordlist(answer,True))

df = pd.DataFrame([preprocessed_column])
df = df.transpose()
df_new = pd.concat([train["question4"],df],axis=1)
df_new.to_csv("test1.csv", index=False, header=["question4","question4 preprocessed"])
"""