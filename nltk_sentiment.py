import pandas as pd
import numpy as np
#from sklearn.model_selection import train_test_split

import re
import nltk
from nltk.corpus import stopwords
from nltk.classify import SklearnClassifier
from sklearn.cross_validation import train_test_split


from wordcloud import WordCloud,STOPWORDS
import matplotlib.pyplot as plt

data = pd.read_csv("res/GEM.tsv",header=0,delimiter="\t",quoting=3)
data = data[['question1','issue spotting']]
q1_data,test = train_test_split(data,test_size = 0.1)


data_1 = q1_data[q1_data['issue spotting']==1]
data_0 = q1_data[q1_data['issue spotting']==0]
"""
data_1 = data_1[['question2','question3']]
data_0 = data_0[['question2','question3']]
"""
data_1 = data_1[['question1']]
data_0 = data_0[['question1']]

def wordcloud_draw(data,img_name, color = 'black'):

    #data1 = re.sub("[^a-zA-Z]", " ", data)
    """
    for row in data:
        if type(row)==float:
            print(type(row))
            row = str(row)
            print(type(row))
    """
    words = ' '.join(str(row) for row in data)
    cleaned_word = " ".join([word for word in words.split()])
    wordcloud = WordCloud(stopwords=STOPWORDS,
                      background_color=color,
                      width=2500,
                      height=2000
                     ).generate(cleaned_word)
    plt.figure(1,figsize=(13, 13))
    plt.imshow(wordcloud)
    plt.axis('off')
    plt.savefig(img_name)
    plt.show()

sentences = []
stopwords_set = set(stopwords.words("english"))
for index, row in q1_data.iterrows():
    #words_cleaned = [e.lower() for e in row.to_string().split()]
    words_cleaned = [e.lower() for e in str(row['question1']).split()]
    words_without_stopwords = [word for word in words_cleaned if not word in stopwords_set]
    sentences.append((words_without_stopwords, row['issue spotting']))

test_1 = test[ test['issue spotting'] == 1]
test_1 = test_1['question1']
test_0 = test[ test['issue spotting'] == 0]
test_0 = test_0['question1']



# Extracting word features
def get_words_in_tweets(tweets):
    all = []
    for (words, sentiment) in tweets:
        all.extend(words)
    return all

def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    features = wordlist.keys()
    return features
w_features = get_word_features(get_words_in_tweets(sentences))

def extract_features(document):
    document_words = set(document)
    features = {}
    for word in w_features:
        features['contains(%s)' % word] = (word in document_words)
    return features

print(w_features)
#wordcloud_draw(w_features,'res/test_img.png')

# Training the Naive Bayes classifier
training_set = nltk.classify.apply_features(extract_features,sentences)
classifier = nltk.NaiveBayesClassifier.train(training_set)

neg_cnt = 0
pos_cnt = 0
for obj in test_0:
    if type(obj)==float:
        obj=str(obj)
    res = classifier.classify(extract_features(obj.split()))
    if (res == 0):
        neg_cnt = neg_cnt + 1
for obj in test_1:
    if type(obj)==float:
        obj=str(obj)
    res = classifier.classify(extract_features(obj.split()))
    if (res == 1):
        pos_cnt = pos_cnt + 1

print('[Negative]: %s/%s ' % (len(test_0), neg_cnt))
print('[Positive]: %s/%s ' % (len(test_1), pos_cnt))
