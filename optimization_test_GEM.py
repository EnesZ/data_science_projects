import pandas as pd
from nltk.corpus import stopwords
#for regular expressions
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer
import re

porter_stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()

train = pd.read_csv("res/GEM.tsv",header=0,delimiter="\t",quoting=3)

#print column names
#print(train.columns.values)

def review_wordlist(review, remove_stopwords=False):
    # Removing non-letter.
    review_text = re.sub("[^a-zA-Z]", " ", review)
    # Converting to lower case and splitting
    words = review_text.lower().split()
    # Optionally remove stopwords
    if remove_stopwords:
        stops = set(stopwords.words("english"))
        words = [w for w in words if not w in stops]
    # Stemming
    words1 = [porter_stemmer.stem(w) for w in words]
    # Lemming
    words2 = [wordnet_lemmatizer.lemmatize(w) for w in words1]
    # list of words to string
    sentence = ' '.join(word for word in words2)
    return (sentence)

def get_preprocessed_column(colunm_name):
    preprocessed_column=[]
    for answer in train[colunm_name]:
        if type(answer) == float:
            answer = str(answer)
        preprocessed_column.append(review_wordlist(answer,True))
    return preprocessed_column

prepr_q1=get_preprocessed_column("question1")
prepr_q2=get_preprocessed_column("question2")
prepr_q3=get_preprocessed_column("question3")
prepr_q4=get_preprocessed_column("question4")

df_q1 = pd.DataFrame([prepr_q1]).transpose()
df_q2 = pd.DataFrame([prepr_q2]).transpose()
df_q3 = pd.DataFrame([prepr_q3]).transpose()
df_q4 = pd.DataFrame([prepr_q4]).transpose()

df_new = pd.concat([train.iloc[:,0:10],df_q1,df_q2,df_q3,df_q4,train.iloc[:,14:20]],axis=1)
df_new.to_csv("GEM_preprocessed.csv", index=False,sep="\t", header=['submission_id','argument evaluation','argument identification',
 'argumentation', 'citations', 'clarity', 'context' ,'feedback', 'implications',
 'issue spotting' ,'question1', 'question2', 'question3', 'question4',
 'question5', 'question6', 'recording clarity' ,'submission' ,'support',
 'writing'])
