from bs4 import BeautifulSoup
import urllib.request
from selenium import webdriver
import time
import pandas as pd

url = 'https://www.klix.ba/vijesti/bih/hoce-li-se-lideri-stranaka-skrasiti-u-domu-naroda-bih-izetbegovic-radoncic-i-covic-cekaju-funkcije/190216075'
#url = 'http://www.pmf.unsa.ba/'

# run firefox webdriver from executable path of your choice
driver = webdriver.Firefox(executable_path='C:/Users/Hp/Documents/res/geckodriver.exe')
#driver = webdriver.Firefox()
#get web page
driver.get(url)
# execute script to scroll down the page

i=0
while i<20:
    driver.execute_script("var scrollingElement = (document.scrollingElement || document.body);"
                      "scrollingElement.scrollTop = scrollingElement.scrollHeight;")
    #driver.execute("window.scrollBy(0,1);scrolldelay = setTimeout(pageScroll,1);")
    # sleep for 30s
    time.sleep(3)
    i+=1
    print(i)

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def get_elem_by_class_name(class_name):
    elements = WebDriverWait(driver, 10).until(
        EC.presence_of_all_elements_located((By.CLASS_NAME, class_name))
    )
    return elements
def make_text_from_elem(elements):
    text_list=[]
    for elem in elements:
        text_list.append(elem.text)
        print(elem.text)
    return text_list
try:
    comments = get_elem_by_class_name("komentarTxt")
    vote_up = get_elem_by_class_name("voteUp")
    vote_down = get_elem_by_class_name("voteDown")
    comments_list = make_text_from_elem(comments)
    vote_up_list = make_text_from_elem(vote_up)
    vote_down_list = make_text_from_elem(vote_down)
finally:
    driver.quit()

new_comments_list=[]

for comment in comments_list:
    list = comment.split()
    new_sentence = ' '.join(word for word in list)
    new_comments_list.append(new_sentence)

df_1 = pd.DataFrame([new_comments_list]).transpose()
df_2 = pd.DataFrame([vote_up_list]).transpose()
df_3 = pd.DataFrame([vote_down_list]).transpose()


df = pd.concat([df_1,df_2,df_3],axis=1)
print(df)

df.to_csv("out/comments_klix",index=False,sep="\t",header=['comment','vote_up','vote_down'])

"""
quote_page = 'https://www.klix.ba/vijesti/bih/hoce-li-se-lideri-stranaka-skrasiti-u-domu-naroda-bih-izetbegovic-radoncic-i-covic-cekaju-funkcije/190216075'

# query the website and return the html to the variable ‘page’
#page = urllib.request.urlopen(quote_page)
req = urllib.request.Request(quote_page,headers={'User-Agent': 'Mozilla/5.0'})
page = urllib.request.urlopen(req).read()

# parse the html using beautiful soup and store in variable `soup`
soup = BeautifulSoup(page,"html.parser")

#finding comments

comments_box = soup.find('div',attrs={'class':'komentarTxt'})
#comments = comments_box.text.strip()
print(soup)
"""