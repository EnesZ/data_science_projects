import pandas as pd
import re

data = pd.read_csv("res/fifa.csv")

data.info()

#fill nan with empty
data["Club"].fillna('empty',inplace = True)

# first few rows
data.head()

print(data.shape)

#number of unique elements
#data.nunique()

#check for NaN values
print(data.isnull().any())

#columns
print(data.columns)

# dataframe from choosen columns
d1 = pd.DataFrame(data,columns=['Age','Overall'])

# random sample
data.sample(5)

# sorting
eldest_1 = data.sort_values(by='Age',ascending=False)[['Name','Age']]
#print(eldest_1.head(10))

# locate some values
some_clubs = ['Juventus', 'Real Madrid']
df_clubs1 = data.loc[data['Club'].isin(some_clubs)][['ID','Age','Club']]

df1 = data.loc[data['Age']>35 ,['ID','Age','Club']]
#print(df1)

# create new colum, drop missing values, using map and lambda,
# lstrips strips the leading space
data['Join_year'] = data['Joined'].dropna().map(lambda x: x.split(' ')[2].lstrip())
#print(data['Join_year'].head())

# group
d2 = data.groupby(['Club'])['Age'].sum().sort_values(ascending = False)
#print(d2.head())

# Top three features per position
#for i, val in data.groupby(data['Position'])[data.iloc[:,55:60].columns].mean().iterrows():
    #print('Position {}: {}, {}, {}'.format(i, *tuple(val.nlargest(3).index)))

d2 = data.groupby(['Club'])['Age'].nlargest(5)
#print(d2)
d2 = data[['Name','Overall','Club']]
grouped_dict = d2.groupby('Club').groups
some_clubs = ('Juventus', 'Real Madrid','FC Barcelona', 'Manchester United')
for key in some_clubs:
    indices = grouped_dict[key]
    print(d2.loc[[*indices]].sort_values(by='Overall',ascending=False).head(5))

    #print('Club {} player {}'.format(club,data.loc[age.index,'Name']))

#print(data[data['Overall']==data['Overall'].max()][['Name','Overall']])
    #print(data[data['Overall']==data['Overall'].min()][['Overall','Name','Nationality','Age']]
#                                            .to_string(index=False))
#print(data['Age'].corr(data['']))
d = data[data['Overall']==92]['Value'].to_string(index =False)
d1 = data['Value'].to_string(index=False)

#list = [float(re.sub("[^0-9_.]","",r)) for r in d1.split()]
#data['value_float'] = list
#print(data['Overall'][:10].corr(data['value_float'][:10]))
#print(data[['Overall','Value','value_float']].head())
#data.assign(float_value = float(re.sub("[^0-9_.]","",d1)))
#print(data['Overall'].corr(data['float_value']))
#print(data.columns.get_loc("Crossing"))
#print(data.columns.get_loc("GKReflexes"))

#for i in range(54, 88):
#    print('{:<15} {}'.format(data.columns[i],data['Overall'].corr(data.iloc[:,i])))

#print(data['Overall'].corr(data['Crossing']))
"""
min_age = data['Age'].min()
max_age = data['Age'].max()

for i in range(min_age,max_age):
    # all players who has i age
    d1 = data[data['Age']==i]
    # player who has the best overall
    d2 = d1[d1['Overall']==d1['Overall'].max()]
    # if we have more players
    name = d2['Name']
    if not name.empty:
        name_list = name.to_string(index=False).split('\n')
        if len(name_list)==1:
            print("The best overall player who has {} years is {} and his overall is {}".format(\
                        i,name_list[0],d2['Overall'].head(1).to_string(index=False)))
        elif len(name_list)>1:
            name = ''.join(n for n in name_list)
            print("The best overall players who have {} years are {} and theirs overall is {}".format( \
                i,name, d2['Overall'].head(1).to_string(index=False)))
    else:
        print("We don't have player who has {} years".format(i))
"""
string_value = data['Value'].to_string(index=False)

# subtract everything except numbers 0-9 and point
#value_list = [float(re.sub("[^0-9_.]","",row)) for row in string_value.split()]
