import pandas as pd
from nltk.corpus import stopwords
#for regular expressions
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer
import re


porter_stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()

train = pd.read_csv("res/training_df_updated_nonan_implicit.csv",header=0)
#print column names
print(train.columns.values)

def review_wordlist(review, remove_stopwords=False):
    # Removing non-letter.
    review_text = re.sub("[^a-zA-Z]", " ", review)
    # Converting to lower case and splitting
    words = review_text.lower().split()
    # Optionally remove stopwords
    if remove_stopwords:
        stops = set(stopwords.words("english"))
        words = [w for w in words if not w in stops]
    # Stemming
    words1 = [porter_stemmer.stem(w) for w in words]
    # Lemming
    words2 = [wordnet_lemmatizer.lemmatize(w) for w in words1]
    # list of words to string
    sentence = ' '.join(word for word in words2)
    return (sentence)

def get_preprocessed_column(colunm_name):
    preprocessed_column=[]
    for answer in train[colunm_name]:
        if type(answer) == float:
            answer = str(answer)
        preprocessed_column.append(review_wordlist(answer,True))
    return preprocessed_column

prepr_q1=get_preprocessed_column("final_which_formula_and_why")
prepr_q2=get_preprocessed_column("final_why_formulas_useful")
prepr_q3=get_preprocessed_column("final_final_choice")


df_q1 = pd.DataFrame([prepr_q1]).transpose()
df_q2 = pd.DataFrame([prepr_q2]).transpose()
df_q3 = pd.DataFrame([prepr_q3]).transpose()

n1 = train.columns.get_loc('final_final_choice')
n2 = train.columns.get_loc('final_which_formula_and_why_formula')
n3 = train.columns.get_loc('final_why_formulas_useful_formula')
n4 = train.columns.get_loc('Relevance_in_mastery')
df_new = pd.concat([train.iloc[:,0:n1],
                    df_q3,train.iloc[:,n1+1:n1+2],df_q1,train.iloc[:,n2:n2+1],df_q2,
                    train.iloc[:,n3:n4+1]],axis=1)
df_new.to_csv("jake_preprocessed.csv", index=False,sep=",", header=train.columns.values)
