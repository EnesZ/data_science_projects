import pandas as pd
import numpy as np
#from sklearn.model_selection import train_test_split
from sklearn.cross_validation import train_test_split
import re
import nltk
from nltk.corpus import stopwords
from nltk.classify import SklearnClassifier

from wordcloud import WordCloud,STOPWORDS
import matplotlib.pyplot as plt

data = pd.read_csv("res/GEM.tsv",header=0,delimiter="\t",quoting=3)
q23_data = data[['question2','question3','argument evaluation']]

data_1 = q23_data[q23_data['argument evaluation']==1]
data_0 = q23_data[q23_data['argument evaluation']==0]
"""
data_1 = data_1[['question2','question3']]
data_0 = data_0[['question2','question3']]
"""
data_1 = data_1['question2']+data_1['question3']
data_0 = data_0['question2']+data_0['question3']

def wordcloud_draw(data,img_name, color = 'black'):

    #data1 = re.sub("[^a-zA-Z]", " ", data)
    """
    for row in data:
        if type(row)==float:
            print(type(row))
            row = str(row)
            print(type(row))
    """
    words = ' '.join(str(row) for row in data)
    cleaned_word = " ".join([word for word in words.split()])
    wordcloud = WordCloud(stopwords=STOPWORDS,
                      background_color=color,
                      width=2500,
                      height=2000
                     ).generate(cleaned_word)
    plt.figure(1,figsize=(13, 13))
    plt.imshow(wordcloud)
    plt.axis('off')
    plt.savefig(img_name)
    plt.show()


"""
print("Positive words")
wordcloud_draw(data_1,'q2_q3_1.png','white')
print("Negative words")
wordcloud_draw(data_0,'q2_q3_0.png','black')
"""

